

module.exports = function(grunt){
    //grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.initConfig({
        sass : {
            all: {
                files: [{
                    expand: true, 
                    cwd: 'assets/sass', 
                    src: ['!**/_*.scss', '**/*.scss'], 
                    dest: 'assets/css', 
                    ext: '.css'
                }],
                // options: [{
                //     compass: true
                // }]
            },

        },
        // compass: {                  // Task
        //     dist: {                   // Target
        //       options: {              // Target options
        //         sassDir: 'assets/sass',
        //         cssDir: 'assets/css',
        //         environment: 'production'
        //       }
        //     },
        // },
        watch: {
            //options: {livereload: true},
            sass: {
                files: ['assets/sass/**/*.{sass,scss}'], 
                tasks: ['sass:all']
            },
            //compass: {files: ['assets/sass/**/*.{sass,scss}'], tasks: ['compass:dist']},
        }
    })

    require('load-grunt-tasks')(grunt);//, {pattern: ['grunt-*', '@*/grunt-*', '!gobble-sass']}*/);
};