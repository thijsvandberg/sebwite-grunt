# readme

Build from Dockerfile
```
docker build -t thijsvandberg/sebwite-grunt .
```

Run container
```
docker run -it  --volume="$PWD/src":/src --name "sebwite-grunt" thijsvandberg/sebwite-grunt
```


Install npm dependencies based on `package.json`
```
npm install
```

Use grunt
```
grunt -h
grunt watch
```

## Node Sass
```
# Delete Grunt Ruby Sass taskrunner
#npm uninstall grunt-contrib-compass --save-dev
npm install grunt-sass --save-dev
```


## Ruby Sass _don't use_
```
gem install sass compass

# Delete Grunt Node Sass taskrunner
npm uninstall grunt-sass --save-dev
# Install ruby sass and compass
npm install grunt-contrib-compass --save-dev
```