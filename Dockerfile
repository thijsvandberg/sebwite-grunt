FROM ubuntu:14.04

######################
## Install Packages ##
######################

# Add repositories & update APT
#RUN add-apt-repository ppa:ondrej/php
RUN apt-get update

# Base stuff
RUN apt-get install -y --force-yes curl build-essential

# Apache, NGINX
RUN apt-get install -y --force-yes git
#RUN apt-get install -y --force-yes nginx php7.0 php7.0-mysql php7.0-fpm php7.0-cli
#RUN apt-get install -y --force-yes apache2 php5 php5-mysql php5-fpm php5-cli php5-sqlite php5

# NodeJS 5.*
RUN curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
RUN apt-get install -y --force-yes nodejs


#EXPOSE 80 443
#VOLUME ["/var/www", "/var/log/apache2", "/etc/apache2"]
#ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

# Install grunt & node-sass
	RUN npm install -g grunt-cli node-sass

WORKDIR /src